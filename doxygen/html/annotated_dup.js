var annotated_dup =
[
    [ "Buffer", "structBuffer.html", "structBuffer" ],
    [ "Font", "structFont.html", "structFont" ],
    [ "Glyph", "structGlyph.html", "structGlyph" ],
    [ "NamePair", "structNamePair.html", "structNamePair" ],
    [ "Options", "structOptions.html", "structOptions" ],
    [ "PARAMS", "structPARAMS.html", "structPARAMS" ],
    [ "Table", "structTable.html", "structTable" ],
    [ "TableRecord", "structTableRecord.html", "structTableRecord" ]
];
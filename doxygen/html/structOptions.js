var structOptions =
[
    [ "bitmap", "structOptions.html#ad8b151e3beb19b46d436988c2dac50d7", null ],
    [ "blankOutline", "structOptions.html#aecc87f00f152fb15d71b57a93d2c74a9", null ],
    [ "cff", "structOptions.html#ae0a7c82ee5165ec1ffcffee3bef2c8d9", null ],
    [ "gpos", "structOptions.html#a39b730960825867b99638543da5aeaf3", null ],
    [ "gsub", "structOptions.html#a6f1a9f65153ff6e2adfcfd686f2f2bbe", null ],
    [ "hex", "structOptions.html#affaa59a6b657a27b50885300a57b68f3", null ],
    [ "nameStrings", "structOptions.html#a734b0bb9a4b59a78af9647def23d0a26", null ],
    [ "out", "structOptions.html#ac538c2ecf1d42f1625ba42124889fefc", null ],
    [ "pos", "structOptions.html#abffb3fb1e4b15dd92f727dca7019013b", null ],
    [ "truetype", "structOptions.html#a59eb261d93d77fe8d996cec60942372b", null ]
];